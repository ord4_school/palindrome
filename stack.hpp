// (c) 2017 Orion Davis
// All rights reserved

#pragma once

class Stack {
private:
    struct Node {
        char data;
        Node *next;
    };

    int size;
    Node *top;

public:
    // Constructor 
    Stack();

    // Destructor deletes all elements in the list
    ~Stack();

    // Add a character to the top of the stack
    // @param: char to add
    // @return: none
    void push(const char&);

    // Remove the top item from the stack and return the item
    // @param: none
    // @return: char from stack that was just removed from the top
    char pop();

    // Check if there are any elements in the stack
    // @param: none
    // @return: true/false if there are no elements in the stack
    bool is_empty();

    // Get the current number of nodes in the stack
    // @param: none
    // @return: number of nodes currently in the stack
    int get_size();

    // Check what is on top of the stack without removing the item
    // @param: none
    // @return: char that is currently at the top of the stack 
    char get_top();
};
