#ifndef PALINDROMER_HPP
#define PALINDROMER_HPP

#include <string>

class PalindromeR {

public:
    // Default constructor, handled by compiler
    PalindromeR() = default;

    // Check if the passed string is a palindrome
    // @param: string to be checked
    // @return: -1 if palindrome, or 1 if not
    int test_string(const std::string& str); 

    // Recursive function to check the ends of a string for equality
    // @param: string to check, and int for the start and end of characters to compare
    // @return: true/false if the passed edges are the same
    bool check_string_for_palindrome(const std::string&, int, int);
};

#endif
