#ifndef PALINDROMEI_HPP
#define PALINDROMEI_HPP

#include "stack.hpp"
#include "queue.hpp"

#include <string>

class PalindromeI {                
public:
    // Default construtor
    PalindromeI() = default;
    
    // Check whether or not a string is a palindrome (alpha characters only)
    // @param: string to check
    // @return: -1 if the string is a palindrome, otherwise the number of matching characters
    int test_string(const std::string&); 
};

#endif
