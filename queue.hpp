// (c) 2017 Orion Davis
// All rights reserved


#pragma once

class Queue {
private:
    char *queue_ptr;
    int size;
    int front;
    int rear;
    int elements;

public:
    // Constructor takes in int parameter which indicates the size of the queue
    Queue(int);

    // Destructor for when the queue is no longer needed
    ~Queue();
    
    // Take a character and put it into the queue
    // @param: char to be added
    // @return: none
    void enqueue(char);

    // Remove a character from the front of the queue
    // @param: none
    // @return: char at the front
    char dequeue();

    // Check if the queue is empty
    // @param: none
    // @return: true/false if there are no more characters in queue
    bool is_empty();      

    // Get the current elements within the queue
    // @param: none
    // @return: number of elements currently in the array
    int get_size();

private:
    char arr[];
};
