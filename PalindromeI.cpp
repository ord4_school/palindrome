// (c) 2017 Orion Davis
// All rights reserved

#include "PalindromeI.hpp"

#include <cctype>
#include <iostream>

int PalindromeI::test_string(const std::string& str) {
    // Build the string to test by ignoring all none letters and making everything lowercase
    std::string test_str = "";
    for (int i = 0; i < str.size(); ++i) {
        if (isalnum(str[i])) {
            test_str += tolower(str[i]);
        }
    }
    
    Stack s;
    Queue q(test_str.size());

    int match_counter = 0;

    // Add all the characters of the normalized string to the data structures
    for (int i = 0; i < test_str.size(); ++i) {
        s.push(test_str[i]);
        q.enqueue(test_str[i]);
    }

    int new_leng = q.get_size();

    // Remove the top of the structures and if they match incremment the matc counter
    // Else, return the number of matches and end the function call
    for (int i = 0; i < new_leng; ++i) {
        if(s.pop() == q.dequeue()) {
            match_counter++;
        }
        else {
            return match_counter;
        }
    }

    return -1;
}
