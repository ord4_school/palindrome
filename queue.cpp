// (c) 2017 Orion Davis
// All rights reserved

#include "queue.hpp"

#include <iostream>

Queue::Queue(int s) {
    size = s;
    arr[size];
    queue_ptr = arr;
    front = 0;
    rear = 0;
    elements = 0;
}

Queue::~Queue() {
}

void Queue::enqueue(char c) {
    if (is_empty()) {
        front = rear = 0;
        queue_ptr[rear] = c;
        rear = (rear + 1) % size;
    }
    else if (elements < size) {
        queue_ptr[rear] = c;
        rear = (rear + 1) % size;
        elements++;
    }
    else {
        throw "Full queue.\n";
    }
}

char Queue::dequeue() {
    if (is_empty()) {
        throw "Empty queue.\n";
    }
    else {
        //std::cout << front << ' ';
        char item = queue_ptr[front];
        //std::cout << item << '\n';
        front = (front + 1) % size;
        elements--;
        return item;
    }
}

bool Queue::is_empty() {
    return elements == 0;
}

int Queue::get_size() {
    return elements;
}
