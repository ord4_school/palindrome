// (c) 2017 Orion Davis
// All rights reserved

#include "PalindromeR.hpp"

#include <cctype>

int PalindromeR::test_string(const std::string& str) {
    // Build a string to test ignoring all non alpha characters and making everything lowercase
    std::string test_str = "";
    for (int i = 0; i < str.size(); ++i) {
        if (isalpha(str[i])) {
            test_str += tolower(str[i]);
        }
    }

    int string_length = test_str.size();

    if (string_length < 2) {
        return -1;
    }

    // Call upon the recursive function to check the edges of the string
    if (check_string_for_palindrome(test_str, 0, string_length - 1)) {
        return -1;
    }
    else {
        return 1;
    }
}

bool PalindromeR::check_string_for_palindrome(const std::string& str, int start, int end) {
    if (start == end) {
        return true;
    }
    else if (str[start] != str[end]) {
        return false;
    }
    if (start < end + 1) {
        // Recursive call shaving the confirmed ends off the string
        return check_string_for_palindrome(str, start + 1, end - 1);
    }

    return true;
}
