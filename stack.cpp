// (c) 2017 Orion Davis
// All rights reserved

#include "stack.hpp"

#include <iostream>

Stack::Stack() {
    top = nullptr;
    size = 0;
}

Stack::~Stack() {
    Node *node_ptr, *next_node;
    node_ptr = top;

    while (node_ptr != nullptr) {
        next_node = node_ptr->next;
        delete node_ptr;
        node_ptr = next_node;
    }
}


void Stack::push(const char& c) {
    Node *new_node = nullptr;
    new_node = new Node;
    new_node->data = c;

    if (is_empty()) {
        top = new_node;
        new_node->next = nullptr;
    }
    else {
        new_node->next = top;
        top = new_node;
    }

    size++;
}

char Stack::pop() {
    Node *temp = nullptr;

    if (is_empty()) {
        throw "Empty stack.\n";
    }
    else {
        char c = top->data;
        temp = top->next;
        delete top;
        top = temp;
        return c;
    }
}

bool Stack::is_empty() {
    return top == nullptr;
}

int Stack::get_size() {
    return size;
}

char Stack::get_top() {
    return top->data;
}
